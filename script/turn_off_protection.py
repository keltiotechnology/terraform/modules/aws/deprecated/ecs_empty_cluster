#!/usr/bin/python
import sys
import boto3
import time
from botocore.config import Config

def queryAllInstances():
    """Query every instances spawned by any autoscaling groups"""
    doneQuery = False
    queriedInstances = []
    next_token = ''
    while doneQuery is False:
        res = client.describe_auto_scaling_instances(MaxRecords = 50, NextToken=next_token)    
        queriedInstances += res['AutoScalingInstances']

        if res.get('NextToken') == None:
            doneQuery = True
        else:
            next_token = res['NextToken']
    return queriedInstances

def isInAutoscalingGroup(instance, ag_name):
    if instance['AutoScalingGroupName'] == ag_name:
        return True
    else:
        return False


if __name__ == "__main__":
    argument = sys.argv[1]    
    read_input = argument.split(",")

    data = dict()
    for el in read_input:
        temp = el.split("=")
        data[temp[0]] = temp[1]

    if data.get('autoscaling_group_name') == None:
        raise Exception("Missing autoscaling_group_name")
    
    if data.get('region') == None:
        raise Exception("Missing region")
    
    autoscaling_group_name = data['autoscaling_group_name']
    region = data['region']
    
    my_config = Config(region_name = region)
    client = boto3.client('autoscaling', config=my_config)
    
    if len(client.describe_auto_scaling_groups(AutoScalingGroupNames=[autoscaling_group_name])['AutoScalingGroups']) == 0:
        print("No provided autoscaing found. It might be not exist or deleted!")
        exit(0)

    print("Disable attribute NewInstancesProtectedFromScaleIn from autoscaling group")
    response = client.update_auto_scaling_group(AutoScalingGroupName=autoscaling_group_name, NewInstancesProtectedFromScaleIn=False)
    time.sleep(0.5)

    print("Start disabling ProtectedFromScaleIn for all attached instances")
    attached_instances = queryAllInstances()
    filteredInstances = list(filter(lambda seq: isInAutoscalingGroup(seq, autoscaling_group_name), attached_instances))

    for instance in filteredInstances:
        instance_id = instance['InstanceId']
        response = client.set_instance_protection(
            InstanceIds=[
                instance_id,
            ],
            AutoScalingGroupName = autoscaling_group_name,
            ProtectedFromScaleIn = False
        )
        print('Instance {instance} is turned off from ProtectedFromScaleIn'.format(instance=instance_id))

    
    print("Reduce number of instances to zero")
    client.update_auto_scaling_group(AutoScalingGroupName=autoscaling_group_name, MinSize=0, DesiredCapacity=0, NewInstancesProtectedFromScaleIn=False)
    
